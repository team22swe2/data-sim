import paho.mqtt.client as mqtt
import asyncio
import unittest


async def on_connect(client, userdata, flags, rc):
    return rc


def connect():
    client = mqtt.Client()
    client.on_connect = on_connect
    client.username_pw_set("pthwuwhk", "3zjFWC-1THcf")
    await asyncio.gather(client.connect("soldier.cloudmqtt.com", 10520, 60))


class MyTest(unittest.TestCase):
    def test(self):
        self.assertEqual(connect(), 0)
