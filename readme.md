## Data Simulator

Data simulator to run on raspberry pi

Goal is to eventually simulate the following data points/driver safety KPIs:

- driving speed (mph)
- acceleration speed (m/s^2)
- break speed (mph)
- turn speed (mph)
- phone usage (seconds)
- time of day (timestamp)
