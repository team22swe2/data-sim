import random
import time
import concurrent.futures
import paho.mqtt.client as mqtt
import json

# Data Simulator class
# simulates data using multithreading and publishes it to an mqtt broker


class DataSimulator:
    def __init__(self, interval, mqttClient):
        self.interval = interval
        self.mqttClient = mqttClient

    def simulate_speed(self, thread_name):
        print("simulating speed limit on thread: " + str(thread_name))
        random_speed = random.randint(30, 75)
        random_speed_limit = random.randint(30, 75)
        isSpeeding = random_speed > random_speed_limit
        time.sleep(self.interval)
        mqttSpeedJson = json.dumps(
            {"speed": random_speed, "speedLimit": random_speed_limit, "isSpeeding": isSpeeding})
        self.mqttClient.publish("speed", mqttSpeedJson)

    def simulate_phone(self, thread_name):
        print("simulating phone usage on thread: " + str(thread_name))
        random_phone_use = random.randint(1, 180)
        time.sleep(self.interval)
        mqttPhoneJson = json.dumps({"phoneUsage": random_phone_use})
        self.mqttClient.publish("phone", mqttPhoneJson)

    def simulate_turning(self, thread_name):
        print("simulating turning on thread: " + str(thread_name))
        random_turning = random.choice([True, False])
        time.sleep(self.interval)
        mqttTurnJson = json.dumps({"isTurningFast": random_turning})
        self.mqttClient.publish("turning", mqttTurnJson)

    def simulate_acceleration(self, thread_name):
        print("simulating acceleration on thread: " + str(thread_name))
        random_accel = random.choice([True, False])
        time.sleep(self.interval)
        mqttAccelJson = json.dumps({"isAcceleratingFast": random_accel})
        self.mqttClient.publish("acceleration", mqttAccelJson)

    def simulate_braking(self, thread_name):
        print("simulating braking on thread: " + str(thread_name))
        random_braking = random.choice([True, False])
        time.sleep(self.interval)
        mqttBrakeJson = json.dumps({"isBrakingFast": random_braking})
        self.mqttClient.publish("braking", mqttBrakeJson)

    # def change_interval(self, thread_name):
    #     interval_json = json.dumps({"interval": random.randint(1, 5)})
    #     self.mqttClient.publish("interval", interval_json)
    #     time.sleep(self.interval)

    def simulate_data(self):
        while True:
            with concurrent.futures.ThreadPoolExecutor(max_workers=1) as executor:
                executor.map(self.simulate_speed, range(1))
                executor.map(self.simulate_phone, range(1))
                executor.map(self.simulate_turning, range(1))
                executor.map(self.simulate_acceleration, range(1))
                executor.map(self.simulate_braking, range(1))

    def on_connect(self, client, userdata, flags, rc):
        print("Connected with result code "+str(rc))

    def on_message(self, client, userdata, msg):
        print(msg.topic+" "+str(msg.payload))
        if(msg.topic == "interval"):
            print("changing heartbeat interval")
            msg_json = json.loads(msg.payload)
            self.interval = msg_json["interval"]

    def on_publish(self, client, userdata, mid):
        print("data published with message id: " + str(mid))

    def on_log(self, client, userdata, level, buf):
        print("log: " + buf)

    def init_client_callbacks(self):
        self.mqttClient.on_connect = self.on_connect
        self.mqttClient.on_message = self.on_message
        self.mqttClient.on_publish = self.on_publish
        self.mqttClient.on_log = self.on_log

    def init_broker_connection(self):
        self.mqttClient.username_pw_set("pthwuwhk", "3zjFWC-1THcf")
        self.mqttClient.connect("soldier.cloudmqtt.com", 10520, 60)
        time.sleep(2)
        self.mqttClient.subscribe("speed")
        self.mqttClient.subscribe("phone")
        self.mqttClient.subscribe("turning")
        self.mqttClient.subscribe("acceleration")
        self.mqttClient.subscribe("braking")
        self.mqttClient.subscribe("interval")
        time.sleep(2)
        self.mqttClient.loop_start()


client = mqtt.Client()
datasim = DataSimulator(1, client)
datasim.init_client_callbacks()
datasim.init_broker_connection()
datasim.simulate_data()
