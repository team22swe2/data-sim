import paho.mqtt.client as mqtt
import time
# this is a test client for connecting to an mqtt broker
# for more information, see:
# https://github.com/CloudMQTT/python-mqtt-example/blob/master/app.py
# https://pypi.org/project/paho-mqtt/#installation
# http://www.steves-internet-guide.com/publishing-messages-mqtt-client/


def on_publish(client, userdata, mid):
    print("data published with message id: " + str(mid))


def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    # client.subscribe("$SYS/#")

# The callback for when a PUBLISH message is received from the server.


def on_disconnect(client, userdata, flags, rc=0):
    print("disconnected with result code " + str(rc))


def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))


def on_log(client, userdata, level, buf):
    print("log: " + buf)


client = mqtt.Client()
client.on_publish = on_publish
client.on_connect = on_connect
client.on_message = on_message
client.on_log = on_log
client.on_disconnect = on_disconnect
# connect to test mqtt server at cloudmqtt.com
client.username_pw_set("pthwuwhk", "3zjFWC-1THcf")
client.connect("soldier.cloudmqtt.com", 10520, 60)
time.sleep(2)
client.subscribe("test")
client.loop_start()
client.publish("test", "from rpi")
# client.loop_stop()
time.sleep(2)
client.disconnect()

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
# client.loop_forever()
